Title: gnome-desktop/gnome-desktop:3.0 is gone and is now gnome-desktop/gnome-desktop:4[legacy]
Author: Marc-Antoine Perennou <keruspe@exherbo.org>
Content-Type: text/plain
Posted: 21st of March 2022
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: gnome-desktop/gnome-desktop:3.0

Upstream split gnome-desktop into several libraries, bumped the API to 4
and kept the 3.0 around using a new option.
Both slots have file collisions so you need to update to gnome-desktop:4,
reinstall dependents of gnome-desktop:3.0 (and maybe enable the legacy option
if all dependents haven't been ported yet)

    cave resolve gnome-desktop:4 -D gnome-desktop:3.0 -U gnome-desktop:3.0 -x1
