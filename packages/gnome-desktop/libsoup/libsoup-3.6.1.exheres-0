# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require meson

SUMMARY="HTTP library implemented in C"
HOMEPAGE="https://wiki.gnome.org/Projects/${PN}"

LICENCES="LGPL-2"
SLOT="3.0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    brotli [[ description = [ Build with Brotli decompression support ] ]]
    gobject-introspection
    gtk-doc

    vapi [[ requires = [ gobject-introspection ] ]]
"

# network violations
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gi-docgen[>=2021.1] )
    build+run:
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.70.0][gobject-introspection(+)?]
        dev-libs/libpsl[>=0.20]
        net-libs/nghttp2[>=1.50.0]
        sys-libs/zlib
        brotli? ( app-arch/brotli )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
    run:
        dev-libs/glib-networking[ssl(+)]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dautobahn=disabled
    -Dgssapi=disabled
    -Dntlm=disabled
    -Dntlm_auth=false
    -Dpkcs11_tests=disabled
    -Dsysprof=disabled
    -Dinstalled_tests=false
    -Dfuzzing=disabled
    # To make glib-networking a runtime dependency
    # Also ease cross-compiling a bit
    -Dtls_check=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    brotli
    'gobject-introspection introspection'
    'gtk-doc docs'
    vapi
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

