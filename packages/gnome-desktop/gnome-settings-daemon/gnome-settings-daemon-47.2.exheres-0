# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings gtk-icon-cache
require meson [ cross_prefix=true ]

SUMMARY="Settings Daemon"
HOMEPAGE="http://www.gnome.org/"

LICENCES="GPL-2"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    cups
    wayland
    wwan

    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.37.1]
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/glib:2[>=2.70.0]
        dev-libs/nspr
        dev-libs/p11-kit:1
        gnome-desktop/gcr:4[>=3.90.0]
        gnome-desktop/geocode-glib:2.0[>=3.26.3]
        gnome-desktop/gnome-desktop:4[>=3.37.1][legacy]
        gnome-desktop/gsettings-desktop-schemas[>=46]
        gnome-desktop/libgudev
        gnome-desktop/libgweather:4
        gps/geoclue:2.0[>=2.3.1]
        media-libs/fontconfig
        media-libs/libcanberra[providers:gtk3]
        media-sound/pulseaudio[>=2.0]
        net-apps/NetworkManager[>=1.0]
        sys-apps/colord[>=1.4.5]
        sys-apps/upower[>=0.99.12][providers:elogind?][providers:systemd?]
        sys-auth/polkit:1[>=0.114]
        sys-sound/alsa-lib
        x11-apps/xkeyboard-config
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.15.3]
        x11-libs/libnotify[>=0.7.3]
        x11-libs/libX11
        x11-libs/libxkbfile
        x11-libs/libXext
        x11-libs/libXfixes[>=6.0]
        x11-libs/libXi
        (
            x11-libs/libwacom[>=0.7]
            x11-libs/pango[>=1.20.0]
        ) [[ *note = [ no way to disable this unnecessary dependency ] ]]
        cups? ( net-print/cups )
        wayland? ( sys-libs/wayland )
        wwan? (
            net-wireless/ModemManager
        )
    recommendation:
        sys-apps/iio-sensor-proxy [[ description = [ accelerometer and rotation handling ] ]]
        sys-apps/power-profiles-daemon [[ description = [ respect active power profile ] ]]
        sys-apps/udisks:2 [[ description = [ support for disk management ] ]]
        providers:elogind? ( sys-auth/elogind[>=209] ) [[ description = [ support power management functionality ] ]]
        providers:systemd? ( sys-apps/systemd[>=243] ) [[ description = [ support power management functionality ] ]]
"

MESON_SOURCE="${WORKBASE}/${PNV/_/.}"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dalsa=true'
    '-Dcolord=true'
    '-Dgudev=true'
    '-Drfkill=true'
    '-Dsmartcard=true'
    '-Dusb-protection=true'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'cups'
    'providers:elogind elogind'
    'providers:systemd systemd'
    'wayland'
    'wwan'
)

# requires X
RESTRICT="test"

src_install() {
    meson_src_install

    keepdir "/etc/gnome-settings-daemon/xrandr"
}

pkg_postinst() {
    gsettings_exlib_compile_gsettings_schemas
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_exlib_compile_gsettings_schemas
    gtk-icon-cache_pkg_postrm
}

