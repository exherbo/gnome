# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings
require vala [ vala_dep=true vala_slots=[ 0.56 ] ]
require gsettings gtk-icon-cache
require meson

SUMMARY="Keep track of time"
DESCRIPTION="
A simple and elegant clock application.
It includes world clocks, alarms, a stopwatch, and timers.
"
HOMEPAGE="https://apps.gnome.org/app/org.gnome.clocks/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.22]
        vala_abis:0.56? ( dev-lang/vala:0.56[>=0.56.4] )
    build+run:
        dev-libs/glib:2[>=2.72.0]
        dev-libs/libadwaita:1[>=1.5.0][vapi]
        gnome-desktop/geocode-glib:2.0[>=1]
        gnome-desktop/gnome-desktop:4
        gnome-desktop/libgweather:4[vapi][providers:soup3]
        gps/geoclue:2.0[>=2.4]
        x11-libs/gtk:4.0[>=4.5]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddocs=false
)

pkg_setup() {
    meson_pkg_setup
    vala_pkg_setup
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

