# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gtk-icon-cache gsettings python [ blacklist=["2 3.8 3.9"] multibuild=false ] meson

SUMMARY="A tool to customize GNOME 3 options."
HOMEPAGE="https://wiki.gnome.org/action/show/Apps/Tweaks"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.16]
    build+run:
        dev-libs/libhandy:1[>=1.5][gobject-introspection]
        gnome-desktop/gsettings-desktop-schemas[>=3.33.0]
        gnome-bindings/pygobject:3[>=3.10][python_abis:*(-)?]
        x11-libs/gtk+:3[>=3.14.0][gobject-introspection]
    run:
        dev-libs/glib:2[>=2.78.0][gobject-introspection]
        dev-libs/libadwaita:1[>=1.4.0]
        gnome-bindings/pygobject:3[>=3.46.0]
        gnome-desktop/gobject-introspection:1[>=1.78.0]
        gnome-desktop/gnome-desktop:4[gobject-introspection][legacy]
        gnome-desktop/gnome-shell[>=3.24]
        gnome-desktop/gnome-settings-daemon:3.0
        gnome-desktop/gsettings-desktop-schemas[>=46.0]
        gnome-desktop/libsoup:2.4[gobject-introspection]
        gnome-desktop/libgudev[>=238]
        gnome-desktop/mutter
        x11-libs/gtk:4.0[>=4.10.0]
        x11-libs/libnotify[gobject-introspection]
        x11-libs/pango[gobject-introspection]
    suggestion:
        gnome-desktop/gnome-shell-extensions [[
            description = [ The user-theme extension is required for setting gnome-shell themes ]
        ]]
"

MESON_SOURCE="${WORKBASE}/${PNV/_/.}"

src_prepare() {
    meson_src_prepare

    # Fix installed script shebangs to use configured python version for target
    edo sed -e "s:/usr/bin/env python3:/usr/$(exhost --target)/bin/python$(python_get_abi):" -i \
        gnome-tweaks
}

src_compile() {
    python_disable_pyc
    meson_src_compile
}

src_install() {
    meson_src_install
    python_bytecompile
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

