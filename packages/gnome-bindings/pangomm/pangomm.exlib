# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ] meson

export_exlib_phases src_prepare

SUMMARY="C++ bindings for pango"
HOMEPAGE="https://www.gtkmm.org/"

LICENCES="LGPL-2.1"
MYOPTIONS="
    disable-deprecated [[ description = [ Omit deprecated API from the library ] ]]
    doc
"

UPSTREAM_DOCUMENTATION="
    http://library.gnome.org/devel/pangomm/stable/ [[
        lang = en
        description = [ Reference manual ]
    ]]
    http://library.gnome.org/devel/pangomm/stable/deprecated.html [[
        lang = en
        description = [ Deprecated API List ]
    ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.5]
        virtual/pkg-config[>=0.20]
        doc? (
            app-doc/doxygen
            dev-lang/perl:*[>=5.6.0]
            dev-libs/libxslt
            media-gfx/graphviz
        )
    build+run:
        dev-libs/glib:2
"

if ever at_least 2.52.0; then
    DEPENDENCIES+="
        build+run:
            gnome-bindings/glibmm:2.68[>=2.68.0]
            dev-cpp/cairomm:1.16[>=1.15.1]
            dev-cpp/libsigc++:3[>=3.0.0]
            x11-libs/pango[>=1.54.0]
    "
else
    DEPENDENCIES+="
        build+run:
            gnome-bindings/glibmm:2.4[>=2.48.0]
            dev-cpp/cairomm:1.0[>=1.12.0]
            dev-cpp/libsigc++:2[>=2.0.0]
            x11-libs/pango[>=1.45.1]
    "
fi

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc build-documentation'
    '!disable-deprecated build-deprecated-api'
)

pangomm_src_prepare() {
    meson_src_prepare

    # Fix docdir
    edo sed \
        -e "/install_docdir/s:/ book_name:/ '${PNVR}':" \
        -i docs/reference/meson.build
}

