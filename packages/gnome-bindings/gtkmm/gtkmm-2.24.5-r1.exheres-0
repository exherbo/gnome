# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="C++ bindings for GTK+"
HOMEPAGE="https://www.gtkmm.org/"

LICENCES="LGPL-2.1"
SLOT="2.4"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    disable-deprecated  [[ description = [ Omit deprecated API from the library ] ]]
    doc
"

UPSTREAM_DOCUMENTATION="
    https://developer-old.gnome.org/gtkmm/stable/ [[
        lang = en
        description = [ Reference manual ]
    ]]
    https://developer-old.gnome.org/gtkmm/2.24/deprecated.html [[
        lang = en
        description = [ Deprecated API list ]
    ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        doc? ( dev-lang/perl:*[>=5.6.0] )
    build+run:
        dev-cpp/cairomm:1.0[>=1.2.2]
        dev-cpp/libsigc++:2[>=2.0.0]
        gnome-bindings/atkmm:1.6[>=2.22.2]
        gnome-bindings/glibmm:2.4[>=2.24.0][disable-deprecated=][doc?]
        gnome-bindings/pangomm:1.4[>=2.27.1]
        x11-libs/gtk+:2[>=2.24.0]
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( '!disable-deprecated deprecated-api' 'doc documentation' )

DEFAULT_SRC_CONFIGURE_PARAMS=( CXXFLAGS="${CXXFLAGS} -std=gnu++11" )

